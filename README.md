
# Titre du Projet

Un site web pour récuperer les récents articles depuis NewsApi.

## Sommaire

- [Technologies Utilisées](#technologies-utilisées)
- [Structure du Projet](#structure-du-projet)
- [Configuration et Installation](#configuration-et-installation)
- [Exécution de l'Application](#exécution-de-lapplication)

## Technologies Utilisées

- **Frontend**: Angular 17
- **Backend**: Spring Boot

## Structure du Projet

Décrivez ici la structure globale de votre projet. Par exemple:

- `/frontend`: Contient le code source de l'application Angular.
  - `/src/app`: Contient les composants, services, etc.
- `/backend`: Contient le code source de l'application Spring Boot.
  - `/src/main/java`: Code source Java pour Spring Boot.
  - `/src/main/resources`: Ressources et configuration pour Spring Boot.

## Configuration et Installation

### Prérequis

- Node.js (version à spécifier)
- Maven (version à spécifier) ou Gradle (version à spécifier)

### Installation

#### Backend

1. Naviguez vers le dossier `backend`.
2. Exécutez `mvn clean install` pour installer les dépendances Maven.
3. Configurez la base de données (si applicable).

#### Frontend

1. Naviguez vers le dossier `frontend`.
2. Exécutez `npm install` pour installer les dépendances Node.js.

## Exécution de l'Application

### Backend

1. Lancez l'application Spring Boot via `mvn spring-boot:run` dans le dossier `backend`.

### Frontend

1. Lancez l'application Angular via `ng serve` dans le dossier `frontend`.
2. Accédez à `http://localhost:4200` dans votre navigateur.